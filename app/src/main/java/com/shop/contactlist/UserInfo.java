package com.shop.contactlist;

public class UserInfo {
    private String id;
    private String fname;
    private String lname;
    private String profilePictureUrl;
    private String phoneNumber;

    public UserInfo(String id, String fname, String lname, String profilePictureUrl, String phoneNumber) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.profilePictureUrl = profilePictureUrl;
        this.phoneNumber = phoneNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
