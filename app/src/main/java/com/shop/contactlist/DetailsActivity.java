package com.shop.contactlist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class DetailsActivity extends AppCompatActivity {
    TextView fname,lname,id,phone;
    CircleImageView profilePicture;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Bundle extras=getIntent().getExtras();
        fname=findViewById(R.id.detailsFname);
        lname=findViewById(R.id.detailsLname);
        id=findViewById(R.id.detailsID);
        phone=findViewById(R.id.detailsPhone);
        profilePicture=findViewById(R.id.detailsProfile);
        fname.append(extras.getString("fname"));
        lname.append(extras.getString("lname"));
        id.append(extras.getString("id"));
        phone.append(extras.getString("phone"));
        Picasso.get().load(extras.getString("profilePictureUrl")).resize(500,500).into(profilePicture);
        profilePicture.setOnLongClickListener(v->{
            CustomDialog dialog = new CustomDialog(this,extras.getString("profilePictureUrl"));
            dialog.show();
            return true;
        });
    }
}
