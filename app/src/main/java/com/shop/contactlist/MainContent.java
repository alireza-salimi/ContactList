package com.shop.contactlist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainContent extends AppCompatActivity {
    TextView drawerName,drawerNumber;
    Toolbar mToolbar;
    DrawerLayout mDrawer;
    NavigationView mNavigation;
    ImageView menuIc;
    ArrayList<UserInfo> arrayList;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    SwipeRefreshLayout refreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_content);
        mToolbar=findViewById(R.id.myToolbar);
        mNavigation=findViewById(R.id.mNavigation);
        mDrawer=findViewById(R.id.mDrawer);
        menuIc=findViewById(R.id.menu_icon);
        recyclerView=findViewById(R.id.recyclerView);
        refreshLayout=findViewById(R.id.refreshLayout);
        drawerName=mNavigation.getHeaderView(0).findViewById(R.id.drawerName);
        drawerNumber=mNavigation.getHeaderView(0).findViewById(R.id.drawerNumber);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        SharedPreferences userInfo=getSharedPreferences("userInfo",MODE_PRIVATE);
        drawerName.setText(userInfo.getString("edit1"," ")+" "+userInfo.getString("edit2"," "));
        drawerNumber.setText(userInfo.getString("edit3"," "));
        menuIc.setOnClickListener(v -> mDrawer.openDrawer(GravityCompat.END));
        mNavigation.setNavigationItemSelectedListener(menuItem -> {
            if(menuItem.getTitle().equals("خروج")){
                SharedPreferences.Editor editor=getSharedPreferences("prefs",MODE_PRIVATE).edit();
                editor.putBoolean("isFirstRun",true);
                editor.apply();
                startActivity(new Intent(this,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
            else {
                mDrawer.closeDrawer(GravityCompat.END);
                Toast.makeText(this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
            }
            return true;
        });
        refreshLayout.setOnRefreshListener(()->getData());
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        layoutManager=new LinearLayoutManager(this);
        arrayList=new ArrayList<>();
        adapter=new ContactsListAdapter(arrayList,this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        getData();
    }
    public void getData(){
        String url="http://116.202.20.190:9000/jason/contacts/";
        JsonArrayRequest arrayRequest=new JsonArrayRequest(Request.Method.GET, url, null, response -> {
            try{
                arrayList.clear();
                for(int i=0;i<response.length();i++){
                    JSONObject object=response.getJSONObject(i);
                    arrayList.add(new UserInfo(
                            object.getString("id"),
                            object.getString("first_name"),
                            object.getString("last_name"),
                            object.getString("image"),
                            object.getString("phone_number")
                    ));
                }
                adapter.notifyDataSetChanged();
            }
            catch (JSONException e){
                e.printStackTrace();
            }
        }, error -> Toast.makeText(this,error.toString(),Toast.LENGTH_SHORT).show());
        if(refreshLayout.isRefreshing())
            refreshLayout.setRefreshing(false);
        RequestQueue requestQueue=Volley.newRequestQueue(this);
        requestQueue.add(arrayRequest);
    }
}