package com.shop.contactlist;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactsListAdapter extends RecyclerView.Adapter<ContactsListAdapter.MyViewHolder> {
    private ArrayList<UserInfo> userInfos;
    private Context context;
    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView fname,lname,phoneNumber;
        CircleImageView profilePicture;
        ConstraintLayout cardViewLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            fname=itemView.findViewById(R.id.fname);
            lname=itemView.findViewById(R.id.lname);
            phoneNumber=itemView.findViewById(R.id.phone);
            profilePicture=itemView.findViewById(R.id.profilePicture);
            cardViewLayout=itemView.findViewById(R.id.cardViewLayout);
        }
    }
    public ContactsListAdapter(ArrayList<UserInfo> userInfos,Context context){
        this.userInfos=userInfos;
        this.context=context;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);
        MyViewHolder viewHolder=new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(userInfos.get(position).getProfilePictureUrl()).resize(2000,2000).into(holder.profilePicture);
        holder.profilePicture.setOnLongClickListener(v -> {
            CustomDialog dialog = new CustomDialog(context,userInfos.get(position).getProfilePictureUrl());
            dialog.show();
            return true;
        });
        holder.fname.setText(userInfos.get(position).getFname());
        holder.lname.setText(userInfos.get(position).getLname());
        holder.phoneNumber.setText(userInfos.get(position).getPhoneNumber());
        holder.cardViewLayout.setOnClickListener(v->{
            Intent intent=new Intent(context,DetailsActivity.class);
            intent.putExtra("fname",userInfos.get(position).getFname())
                    .putExtra("lname",userInfos.get(position).getLname())
                    .putExtra("id",userInfos.get(position).getId())
                    .putExtra("profilePictureUrl",userInfos.get(position).getProfilePictureUrl())
                    .putExtra("phone",userInfos.get(position).getPhoneNumber());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return userInfos.size();
    }
}