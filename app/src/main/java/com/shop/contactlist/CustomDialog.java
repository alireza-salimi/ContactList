package com.shop.contactlist;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class CustomDialog extends Dialog {
    Context context;
    String imageUrl;
    ImageView imageView,close;
    CustomDialog(Context context, String imageUrl){
        super(context);
        this.imageUrl=imageUrl;
        this.context=context;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_dialog);
        imageView = findViewById(R.id.dialogImage);
        close = findViewById(R.id.dialogClose);
        Picasso.get().load(imageUrl).resize(300,500).into(imageView);
        close.setOnClickListener(v->dismiss());
    }
}