package com.shop.contactlist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences,userInfo;
    boolean isFirstTime;
    Button signup,signin;
    TextInputEditText edit1,edit2,edit3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        signup=findViewById(R.id.signup);
        signin=findViewById(R.id.signin);
        sharedPreferences= getSharedPreferences("prefs",MODE_PRIVATE);
        userInfo=getSharedPreferences("userInfo",MODE_PRIVATE);
        isFirstTime=sharedPreferences.getBoolean("isFirstRun",true);
        if(!isFirstTime){
            finish();
            startActivity(new Intent(MainActivity.this,MainContent.class));
        }

        View.OnClickListener onClickListener=view->{
            SharedPreferences.Editor editor=sharedPreferences.edit();
            editor.putBoolean("isFirstRun",false);
            editor.apply();
            Intent intent=new Intent(MainActivity.this,MainContent.class);
            SharedPreferences.Editor infoEditor=userInfo.edit();
            infoEditor.putString("edit1",edit1.getText().toString())
                    .putString("edit2",edit2.getText().toString())
                    .putString("edit3",edit3.getText().toString());
            infoEditor.apply();
            intent.putExtra("edit1",userInfo.getString("edit1","a"))
                    .putExtra("edit2",userInfo.getString("edit2","b"))
                    .putExtra("edit3",userInfo.getString("edit3","b"));
            Toast.makeText(this,"Welcome To Contact List",Toast.LENGTH_SHORT).show();
            finish();
            startActivity(intent);
        };
        edit1=findViewById(R.id.edit1);
        edit2=findViewById(R.id.edit2);
        edit3=findViewById(R.id.edit3);
        signup.setOnClickListener(onClickListener);
        signin.setOnClickListener(onClickListener);
    }
}
